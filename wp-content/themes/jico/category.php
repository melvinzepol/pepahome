<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Jico_Theme
 * @since Jico 1.0
 */
$jico_opt = get_option( 'jico_opt' );
get_header();
$jico_bloglayout = 'grid';
if(isset($jico_opt['blog_layout']) && $jico_opt['blog_layout']!=''){
	$jico_bloglayout = $jico_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$jico_bloglayout = $_GET['layout'];
}
$jico_blogsidebar = 'right';
if(isset($jico_opt['sidebarblog_pos']) && $jico_opt['sidebarblog_pos']!=''){
	$jico_blogsidebar = $jico_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$jico_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$jico_bloglayout = 'nosidebar';
}
$jico_blog_main_extra_class = NULl;
if($jico_blogsidebar=='left') {
	$jico_blog_main_extra_class = 'order-lg-last';
}
$main_column_class = NULL;
switch($jico_bloglayout) {
	case 'nosidebar':
		$jico_blogclass = 'blog-nosidebar';
		$jico_blogcolclass = 12;
		$jico_blogsidebar = 'none';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumb');
		break;
	case 'largeimage':
		$jico_blogclass = 'blog-large';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumbwide');
		break;
	case 'grid':
		$jico_blogclass = 'grid';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumbwide');
		break;
	default:
		$jico_blogclass = 'blog-sidebar';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumb');
}
?>
<div class="main-container <?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ echo 'has-image';} ?>">
	<div class="title-breadcumbs">
		<!-- blog banner -->
		<?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ ?>
			<div class="blog-banner banner-image">
				<img src="<?php echo esc_url($jico_opt['blog_banner']['url']); ?>" alt="<?php esc_attr_e('Shop banner','jico') ?>" />
			</div>
		<?php } ?>
		<!-- end blog banner -->
		<div class="title-breadcumbs-text">
			<div class="container">
				<header class="entry-header">
					<h2 class="entry-title"><?php the_archive_title(); ?></h2>
				</header>
				<div class="breadcrumb-container">
					<div class="container">
						<?php Jico_Class::jico_breadcrumb(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$jico_blogcolclass; ?> <?php echo ''.$main_column_class; ?> <?php echo esc_attr($jico_blog_main_extra_class);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($jico_blogclass); if($jico_blogsidebar=='left') {echo ' left-sidebar'; } if($jico_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>
						<?php if ( category_description() ) : // Show an optional category description ?>
							<div class="archive-header">
								<h3 class="archive-title"><?php printf( esc_html__( 'Category Archives: %s', 'jico' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h3>
									<div class="archive-meta"><?php echo category_description(); ?></div>
							</div>
						<?php endif; ?><!-- .archive-header -->
						<div class="post-container">
							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();
								/* Include the post format-specific template for the content. If you want to
								 * this in a child theme then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							endwhile;
							?>
						</div>
						<?php Jico_Class::jico_pagination(); ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($jico_opt['inner_brand']) && function_exists('jico_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($jico_opt['inner_brand'] && isset($jico_opt['brand_logos'][0]) && $jico_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($jico_opt['inner_brand_title']) && $jico_opt['inner_brand_title']!=''){ ?>
							<div class="heading-title style1 ">
								<h3><?php echo esc_html( $jico_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>