<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Jico_Theme
 * @since Jico 1.0
 */
$jico_opt = get_option( 'jico_opt' );
get_header();
$jico_bloglayout = 'grid';
if(isset($jico_opt['blog_layout']) && $jico_opt['blog_layout']!=''){
	$jico_bloglayout = $jico_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$jico_bloglayout = $_GET['layout'];
}
$jico_blogsidebar = 'right';
if(isset($jico_opt['sidebarblog_pos']) && $jico_opt['sidebarblog_pos']!=''){
	$jico_blogsidebar = $jico_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$jico_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$jico_bloglayout = 'nosidebar';
}
$jico_blog_main_extra_class = NULl;
if($jico_blogsidebar=='left') {
	$jico_blog_main_extra_class = 'order-lg-last';
}
$main_column_class = NULL;
switch($jico_bloglayout) {
	case 'nosidebar':
		$jico_blogclass = 'blog-nosidebar';
		$jico_blogcolclass = 12;
		$jico_blogsidebar = 'none';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumb');
		break;
	case 'largeimage':
		$jico_blogclass = 'blog-large';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumbwide');
		break;
	case 'grid':
		$jico_blogclass = 'grid';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumbwide');
		break;
	default:
		$jico_blogclass = 'blog-sidebar';
		$jico_blogcolclass = 9;
		$main_column_class = 'main-column';
		Jico_Class::jico_post_thumbnail_size('jico-post-thumb');
}
?>
<div class="main-container <?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ echo 'has-image';} ?>">
	<div class="title-breadcumbs">
		<!-- blog banner -->
		<?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ ?>
			<div class="blog-banner banner-image">
				<img src="<?php echo esc_url($jico_opt['blog_banner']['url']); ?>" alt="<?php esc_attr_e('Shop banner','jico') ?>" />
			</div>
		<?php } ?>
		<!-- end blog banner -->
		<div class="title-breadcumbs-text">
			<div class="container">
				<header class="entry-header">
					<h2 class="entry-title"><?php if(isset($jico_opt['blog_header_text']) && ($jico_opt['blog_header_text'] !='')) { echo esc_html($jico_opt['blog_header_text']); } else { esc_html_e('Blog', 'jico');}  ?></h2>
				</header>
				<div class="breadcrumb-container">
					<div class="container">
						<?php Jico_Class::jico_breadcrumb(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$jico_blogcolclass; ?> <?php echo ''.$main_column_class; ?> <?php echo esc_attr($jico_blog_main_extra_class);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($jico_blogclass); ?>">
					<div class="blog-wrapper">
						<?php if ( have_posts() ) : ?>
							<div class="post-container">
								<?php /* Start the Loop */ ?>
								<?php while ( have_posts() ) : the_post(); ?>
									<?php get_template_part( 'content', get_post_format() ); ?>
								<?php endwhile; ?>
							</div>
							<?php Jico_Class::jico_pagination(); ?>
						<?php else : ?>
							<article id="post-0" class="post no-results not-found">
							<?php if ( current_user_can( 'edit_posts' ) ) :
								// Show a different message to a logged-in user who can add posts.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'jico' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'jico' ), array('a'=>array('href'=>array()))), admin_url( 'post-new.php' ) ); ?></p>
								</div><!-- .entry-content -->
							<?php else :
								// Show the default message to everyone else.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'jico' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'jico' ); ?></p>
									<?php get_search_form(); ?>
								</div><!-- .entry-content -->
							<?php endif; // end current_user_can() check ?>
							</article><!-- #post-0 -->
						<?php endif; // end have_posts() check ?>
					</div>
				</div>
			</div>
			<?php if($jico_bloglayout!='nosidebar' && is_active_sidebar('sidebar-1')): ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($jico_opt['inner_brand']) && function_exists('jico_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($jico_opt['inner_brand'] && isset($jico_opt['brand_logos'][0]) && $jico_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($jico_opt['inner_brand_title']) && $jico_opt['inner_brand_title']!=''){ ?>
							<div class="heading-title style1 ">
								<h3><?php echo esc_html( $jico_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>