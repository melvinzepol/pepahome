"use strict";
var jico_magnifier_vars;
var yith_magnifier_options = {
		sliderOptions: {
			responsive: jico_magnifier_vars.responsive,
			circular: jico_magnifier_vars.circular,
			infinite: jico_magnifier_vars.infinite,
			direction: 'left',
			debug: false,
			auto: false,
			align: 'left',
			height: 'auto',
			width: 'auto',
			prev    : {
				button  : "#slider-prev",
				key     : "left"
			},
			next    : {
				button  : "#slider-next",
				key     : "right"
			},
			scroll : {
				items     : 1,
				pauseOnHover: true
			},
			items   : {
				visible: Number(jico_magnifier_vars.visible),
			},
			swipe : {
				onTouch:    true,
				onMouse:    true
			},
			mousewheel : {
				items: 1
			}
		},
		showTitle: false,
		zoomWidth: jico_magnifier_vars.zoomWidth,
		zoomHeight: jico_magnifier_vars.zoomHeight,
		position: jico_magnifier_vars.position,
		lensOpacity: jico_magnifier_vars.lensOpacity,
		softFocus: jico_magnifier_vars.softFocus,
		adjustY: 0,
		disableRightClick: false,
		phoneBehavior: jico_magnifier_vars.phoneBehavior,
		loadingLabel: jico_magnifier_vars.loadingLabel,
	};