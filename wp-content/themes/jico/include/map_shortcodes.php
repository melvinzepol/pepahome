<?php
//Shortcodes for Visual Composer
add_action( 'vc_before_init', 'jico_vc_shortcodes' );
function jico_vc_shortcodes() { 
	//Site logo
	vc_map( array(
		'name' => esc_html__( 'Logo', 'jico'),
		'description' => esc_html__( 'Insert logo image', 'jico' ),
		'base' => 'roadlogo',
		'class' => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params' => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload logo image', 'jico' ),
				'description'=> esc_html__( 'Note: For retina screen, logo image size is at least twice as width and height (width is set below) to display clearly', 'jico' ),
				'param_name' => 'logo_image',
				'value'      => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Insert logo link or not', 'jico' ),
				'param_name' => 'logo_link',
				'value'      => array(
					'Yes'	=> 'yes',
					'No'	=> 'no',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Logo width (unit: px)', 'jico' ),
				'description'=> esc_html__( 'Insert number. Leave blank if you want to use original image size', 'jico' ),
				'param_name' => 'logo_width',
				'value'      => esc_html__( '150', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )                  => 'style1',
					esc_html__( 'Style 2 (footer)', 'jico' )         => 'style2',
					esc_html__( 'Style 3 (demo 3 header)', 'jico' )  => 'style3',
				),
			),
		)
	) );
	//Main Menu
	vc_map( array(
		'name'        => esc_html__( 'Main Menu', 'jico'),
		'description' => esc_html__( 'Set Primary Menu in Apperance - Menus - Manage Locations', 'jico' ),
		'base'        => 'roadmainmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'        => '',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Set Primary Menu in Apperance - Menus - Manage Locations', 'jico' ),
				'description' => esc_html__( 'More settings in Theme Options - Main Menu', 'jico' ),
				'param_name'  => 'no_settings',
				'value'       => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1 (Default)', 'jico' )        => 'style1',
					esc_html__( 'Style 2 (Demo 2+3)', 'jico' )         => 'style2',
					esc_html__( 'Style 3 (Demo 2+3 sticky)', 'jico' )  => 'style3',
				),
			),
		),
	) );
	//Sticky Menu
	vc_map( array(
		'name'        => esc_html__( 'Sticky Menu', 'jico'),
		'description' => esc_html__( 'Set Sticky Menu in Apperance - Menus - Manage Locations', 'jico' ),
		'base'        => 'roadstickymenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'        => '',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Set Sticky Menu in Apperance - Menus - Manage Locations', 'jico' ),
				'description' => esc_html__( 'More settings in Theme Options - Main Menu', 'jico' ),
				'param_name'  => 'no_settings',
				'value'       => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
					esc_html__( 'Style 2', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Mobile Menu
	vc_map( array(
		'name'        => esc_html__( 'Mobile Menu', 'jico'),
		'description' => esc_html__( 'Set Mobile Menu in Apperance - Menus - Manage Locations', 'jico' ),
		'base'        => 'roadmobilemenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'        => '',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Set Mobile Menu in Apperance - Menus - Manage Locations', 'jico' ),
				'description' => esc_html__( 'More settings in Theme Options - Main Menu', 'jico' ),
				'param_name'  => 'no_settings',
				'value'       => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
					esc_html__( 'Style 2', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Categories Menu
	vc_map( array(
		'name'        => esc_html__( 'Categories Menu', 'jico'),
		'description' => esc_html__( 'Set Categories Menu in Apperance - Menus - Manage Locations', 'jico' ),
		'base'        => 'roadcategoriesmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'        => '',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Set Categories Menu in Apperance - Menus - Manage Locations', 'jico' ),
				'description' => esc_html__( 'More settings in Theme Options - Categories Menu', 'jico' ),
				'param_name'  => 'no_settings',
				'value'       => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Show full Categories in home page ?', 'jico' ),
				'description' => esc_html__( 'In inner pages, it only shows the toogle', 'jico' ),
				'param_name' => 'categories_show_home',
				'value'      => array(
					esc_html__( 'No', 'jico' )  => 'false',
					esc_html__( 'Yes', 'jico' ) => 'true',
				),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
					esc_html__( 'Style 2', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Social Icons
	vc_map( array(
		'name'        => esc_html__( 'Social Icons', 'jico'),
		'description' => esc_html__( 'Configure icons and links in Theme Options', 'jico' ),
		'base'        => 'roadsocialicons',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Configure icons and links in Theme Options > Social Icons', 'jico' ),
				'param_name' => 'no_settings',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Social title element', 'jico' ),
				'param_name' => 'social_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Social sub-title element', 'jico' ),
				'param_name' => 'sub_social_title',
				'value'      => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1 (header)', 'jico' ) => 'style1',
					esc_html__( 'Style 2 (footer)', 'jico' ) => 'style2',
				),
			),
		),
	) );
	//Mini Cart
	vc_map( array(
		'name'        => esc_html__( 'Mini Cart', 'jico'),
		'description' => esc_html__( 'Mini Cart', 'jico' ),
		'base'        => 'roadminicart',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'jico' ),
				'param_name' => 'no_settings',
				'value'      => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )              => 'style1',
					esc_html__( 'Style 2 (header mobile)', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Wishlist
	vc_map( array(
		'name'        => esc_html__( 'Wishlist', 'jico'),
		'description' => esc_html__( 'Wishlist', 'jico' ),
		'base'        => 'roadwishlist',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
					esc_html__( 'Style 2', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Products Search without dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (No dropdown)', 'jico'),
		'description' => esc_html__( 'Product Search (No dropdown)', 'jico' ),
		'base'        => 'roadproductssearch',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
					esc_html__( 'Style 2 (Demo 2+3)', 'jico' )         => 'style2',
				),
			),
		),
	) );
	//Products Search with dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (Dropdown)', 'jico'),
		'description' => esc_html__( 'Product Search (Dropdown)', 'jico' ),
		'base'        => 'roadproductssearchdropdown',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'jico' ),
				'param_name' => 'no_settings',
				'value'      => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )               => 'style1',
					esc_html__( 'Style 2 (demo 2 + 3)', 'jico' )  => 'style2',
				),
			),
		),
	) );
	//Latest posts
	vc_map( array(
		'name'        => esc_html__( 'Latest posts', 'jico' ),
		'description' => esc_html__( 'List posts', 'jico' ),
		'base'        => 'latestposts',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of posts', 'jico' ),
				'param_name' => 'posts_per_page',
				'value'      => esc_html__( '10', 'jico' ),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'jico' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'jico' ),
				'description' => esc_html__( 'Slug of the category (example: slug-1, slug-2). Default is 0 : show all posts', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Image scale', 'jico' ),
				'param_name' => 'image',
				'value'      => array(
					'Wide'	=> 'wide',
					'Square'=> 'square',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Excerpt length', 'jico' ),
				'param_name' => 'length',
				'value'      => esc_html__( '20', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns', 'jico' ),
				'param_name' => 'colsnumber',
				'value'      => array(
					'1'	=> '1',
					'2'	=> '2',
					'3'	=> '3',
					'4'	=> '4',
					'5'	=> '5',
					'6'	=> '6',
				),
			),
			array(
				'type'        => 'dropdown',
				'holder'     => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1', 'jico' )           => 'style1',
					esc_html__( 'Style 2 (footer)', 'jico' )  => 'style2',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Enable slider', 'jico' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'jico' ),
				'param_name' => 'rowsnumber',
				'group'      => esc_html__( 'Slider Options', 'jico' ),
				'value'      => array(
						'1'	=> '1',
						'2'	=> '2',
						'3'	=> '3',
						'4'	=> '4',
					),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Number of columns (screen: 1200px - 1499px)', 'jico' ),
				'param_name' => 'items_1200_1499',
				'group'      => esc_html__( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '3', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 992px - 1199px)', 'jico' ),
				'param_name' => 'items_992_1199',
				'value'      => esc_html__( '3', 'jico' ),
				'group'      => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 768px - 991px)', 'jico' ),
				'param_name' => 'items_768_991',
				'value'      => esc_html__( '3', 'jico' ),
				'group'      => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 640px - 767px)', 'jico' ),
				'param_name' => 'items_640_767',
				'value'      => esc_html__( '2', 'jico' ),
				'group'      => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 480px - 639px)', 'jico' ),
				'param_name' => 'items_480_639',
				'value'      => esc_html__( '2', 'jico' ),
				'group'      => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: under 479px)', 'jico' ),
				'param_name' => 'items_0_479',
				'value'      => esc_html__( '1', 'jico' ),
				'group'      => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation', 'jico' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
				'value'       => array(
					esc_html__( 'Yes', 'jico' ) => true,
					esc_html__( 'No', 'jico' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Pagination', 'jico' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
				'value'       => array(
					esc_html__( 'No', 'jico' )  => false,
					esc_html__( 'Yes', 'jico' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Item Margin (unit: pixel)', 'jico' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Slider speed number (unit: second)', 'jico' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Slider loop', 'jico' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Slider Auto', 'jico' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => esc_html__( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'dropdown',
				'holder'      => 'div',
				'heading'     => esc_html__( 'Navigation style', 'jico' ),
				'param_name'  => 'navigation_style',
				'group'       => esc_html__( 'Slider Options', 'jico' ),
				'value'       => array(
					esc_html__( 'Navigation center horizontal', 'jico' )  => 'navigation-style1',
					esc_html__( 'Navigation top-right', 'jico' )          => 'navigation-style2',
				),
			),
		),
	) );
	//Testimonials
	vc_map( array(
		'name'        => esc_html__( 'Testimonials', 'jico' ),
		'description' => esc_html__( 'Testimonial slider', 'jico' ),
		'base'        => 'testimonials',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'jico'),
		"icon"     	  => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of testimonial', 'jico' ),
				'param_name' => 'limit',
				'value'      => esc_html__( '10', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Author', 'jico' ),
				'param_name' => 'display_author',
				'value'      => array(
					'Yes'	=> '1',
					'No'	=> '0',
				),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Avatar', 'jico' ),
				'param_name' => 'display_avatar',
				'value'      => array(
					'Yes'=> '1',
					'No' => '0',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Avatar image size', 'jico' ),
				'param_name'  => 'size',
				'value'       => '150',
				'description' => esc_html__( 'Avatar image size in pixels. Default is 150', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display URL', 'jico' ),
				'param_name' => 'display_url',
				'value'      => array(
					'Yes'	=> '1',
					'No'	=> '0',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'jico' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'jico' ),
				'description' => esc_html__( 'Slug of the category. Default is 0 : show all testimonials', 'jico' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Navigation', 'jico' ),
				'param_name' => 'navigation',
				'value'      => array(
					esc_html__( 'No', 'jico' )  => false,
					esc_html__( 'Yes', 'jico' ) => true,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Pagination', 'jico' ),
				'param_name' => 'pagination',
				'value'      => array(
					esc_html__( 'Yes', 'jico' ) => true,
					esc_html__( 'No', 'jico' )  => false,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    =>  esc_html__( 'Slider speed number (unit: second)', 'jico' ),
				'param_name' => 'speed',
				'value'      => '500',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider loop', 'jico' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider Auto', 'jico' ),
				'param_name' => 'auto',
			),
			array(
				'type'        => 'dropdown',
				'holder'      => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1', 'jico' )                => 'style1',
					esc_html__( 'Style 2 (about page)', 'jico' )   => 'style-about-page',
				),
			),
			array(
				'type'        => 'dropdown',
				'holder'      => 'div',
				'heading'     => esc_html__( 'Navigation style', 'jico' ),
				'param_name'  => 'navigation_style',
				'value'       => array(
					esc_html__( 'Navigation top-right', 'jico' )          => 'navigation-style1',
					esc_html__( 'Navigation center horizontal', 'jico' )  => 'navigation-style2',
				),
			),
		),
	) );
	//Counter
	vc_map( array(
		'name'     => esc_html__( 'Counter', 'jico' ),
		'description' => esc_html__( 'Counter', 'jico' ),
		'base'     => 'jico_counter',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'        => 'attach_image',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Image icon', 'jico' ),
				'param_name'  => 'image',
				'value'       => '',
				'description' => esc_html__( 'Upload icon image', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number', 'jico' ),
				'param_name' => 'number',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Text', 'jico' ),
				'param_name' => 'text',
				'value'      => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
				),
			),
		),
	) );
	//Heading title
	vc_map( array(
		'name'     => esc_html__( 'Heading Title', 'jico' ),
		'description' => esc_html__( 'Heading Title', 'jico' ),
		'base'     => 'roadthemes_title',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading title element', 'jico' ),
				'param_name' => 'heading_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading sub-title element', 'jico' ),
				'param_name' => 'sub_heading_title',
				'value'      => '',
			),
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload heading title image', 'jico' ),
				'param_name' => 'heading_image',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 (Default)', 'jico' )             => 'style1',
					esc_html__( 'Style 2 (Footer title)', 'jico' )        => 'style2',
					esc_html__( 'Style 3 (Tab title)', 'jico' )           => 'style3',
					esc_html__( 'Style 4 (Demo 2)', 'jico' )              => 'style4',
					esc_html__( 'Style 5 (Demo 2 style 2)', 'jico' )      => 'style5',
					esc_html__( 'Style 6 (Demo 3 style 1)', 'jico' )      => 'style6',
					esc_html__( 'Style 7 (Demo 3 style 2)', 'jico' )      => 'style7',
				),
			),
		),
	) );
	//Countdown
	vc_map( array(
		'name'     => esc_html__( 'Countdown', 'jico' ),
		'description' => esc_html__( 'Countdown', 'jico' ),
		'base'     => 'roadthemes_countdown',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (day)', 'jico' ),
				'param_name' => 'countdown_day',
				'value'      => '1',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (month)', 'jico' ),
				'param_name' => 'countdown_month',
				'value'      => '1',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (year)', 'jico' ),
				'param_name' => 'countdown_year',
				'value'      => '2020',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Style', 'jico' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'jico' )  => 'style1',
				),
			),
		),
	) );
	//Mailchimp newsletter
	vc_map( array(
		'name'     => esc_html__( 'Mailchimp Newsletter', 'jico' ),
		'description' => esc_html__( 'Mailchimp Newsletter ', 'jico' ),
		'base'     => 'roadthemes_newsletter',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'        => 'textarea',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Newsletter title', 'jico' ),
				'param_name'  => 'newsletter_title',
				'value'       => '',
			),
			array(
				'type'        => 'textarea',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Newsletter sub-title', 'jico' ),
				'param_name'  => 'newsletter_sub_title',
				'value'       => '',
			),
			array(
				'type'        => 'attach_image',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Upload newsletter title image', 'jico' ),
				'param_name'  => 'newsletter_image',
				'value'       => '',
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Insert id of Mailchimp Form', 'jico' ),
				'description' => esc_html__( 'See id in admin -> MailChimp for WP -> Form, under the form title', 'jico' ),
				'param_name'  => 'newsletter_form_id',
				'value'       => '',
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 (Default)', 'jico' )    => 'style1',
					esc_html__( 'Style 2', 'jico' )              => 'style2',
				),
			),
		),
	) );
	//Custom Menu
	$custom_menus = array();
	if ( 'vc_edit_form' === vc_post_param( 'action' ) && vc_verify_admin_nonce() ) {
		$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
		if ( is_array( $menus ) && ! empty( $menus ) ) {
			foreach ( $menus as $single_menu ) {
				if ( is_object( $single_menu ) && isset( $single_menu->name, $single_menu->term_id ) ) {
					$custom_menus[ $single_menu->name ] = $single_menu->term_id;
				}
			}
		}
	}
	vc_map( array(
		'name'     => esc_html__( 'Custom Menu', 'jico' ),
		'description' => esc_html__( 'Custom Menu', 'jico' ),
		'base'     => 'roadthemes_menu',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload menu image', 'jico' ),
				'param_name' => 'menu_image',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Menu title', 'jico' ),
				'param_name' => 'menu_title',
				'value'      => 'Title',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Choose Menu', 'jico' ),
				'param_name'  => 'nav_menu',
				'value'       => $custom_menus,
				'description' => empty( $custom_menus ) ? __( 'Custom menus not found. Please visit <b>Appearance > Menus</b> page to create new menu.', 'jico' ) : __( 'Select menu to display.', 'jico' ),
				'admin_label' => true,
				'save_always' => true,
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Menu text', 'jico' ),
				'param_name' => 'menu_text',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Menu link text', 'jico' ),
				'param_name' => 'menu_link_text',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Menu link url', 'jico' ),
				'param_name' => 'menu_link_url',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 (Default)', 'jico' )    => 'style1',
				),
			),
		),
	) );
	//Policy
	vc_map( array(
		'name'     => esc_html__( 'Policy', 'jico' ),
		'description' => __( 'Policy content', 'jico' ),
		'base'     => 'roadthemes_policy',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload Policy icon', 'jico' ),
				'param_name' => 'policy_icon',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Policy title', 'jico' ),
				'param_name' => 'policy_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Policy text', 'jico' ),
				'param_name' => 'policy_text',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'holder'     => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 (Default)', 'jico' )    => 'style1',
				),
			),
		),
	) );
	//Static block
	vc_map( array(
		'name'     => esc_html__( 'Static block 1', 'jico' ),
		'description' => esc_html__( 'Static block with link text', 'jico' ),
		'base'     => 'roadthemes_static_1',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload Static image', 'jico' ),
				'param_name' => 'static_image',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static title', 'jico' ),
				'param_name' => 'static_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static text', 'jico' ),
				'param_name' => 'static_text',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static link text', 'jico' ),
				'param_name' => 'static_link_text',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static link url', 'jico' ),
				'param_name' => 'static_link_url',
				'value'      => '#',
			),
			array(
				'type'        => 'dropdown',
				'holder'     => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 ', 'jico' )    => 'style1',
					esc_html__( 'Style 2 ', 'jico' )    => 'style2',
					esc_html__( 'Style 3 ', 'jico' )    => 'style3',
				),
			),
		),
	) );
	vc_map( array(
		'name'     => esc_html__( 'Static block 2', 'jico' ),
		'description' => esc_html__( 'Static block without link text', 'jico' ),
		'base'     => 'roadthemes_static_2',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'jico'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload Static image', 'jico' ),
				'param_name' => 'static_image',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static title', 'jico' ),
				'param_name' => 'static_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static text', 'jico' ),
				'param_name' => 'static_text',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Static link url', 'jico' ),
				'param_name' => 'static_link_url',
				'value'      => '#',
			),
			array(
				'type'        => 'dropdown',
				'holder'     => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 ', 'jico' )    => 'style1',
					esc_html__( 'Style 2 ', 'jico' )    => 'style2',
					esc_html__( 'Style 3 ', 'jico' )    => 'style3',
				),
			),
		),
	) );
}
?>